# UniConnector微服务开发平台(筹备中...)

#### 介绍
UniConnector微服务开发平台，致力于提供技术中台建设技术解决方案与开发工具，使用户或开发者能快速建立起适合企业本身和所处行业领域的数据中台和业务中台，快速开发适应市场的产品。

#### 平台架构

![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/162509_11036127_4801041.png "微服务架构图4.png")

#### 平台组成

1.  UniAI 智能分析
2.  UniAnalysis 分析展示
3.  UniAuth 统一认证平台
4.  UniBI 人工智能
5.  UniData 大数据平台
6.  UniDesigner 微服务设计器
7.  UniETL 数据处理工具
8.  UniFlow 流程设计器
9.  UniGateway API网关
10. UniGenerator 微服务生成器
11. UniMeta 元数据管理
12. UniPlatform 微服务平台
13. UniSchedule 智能调度
14. UniSearch 检索
15. UniStore 微服务库    
16. UniView 统一数据展示
17. UniDevice 统一设备连接器

#### 平台截图

1.  UniDesigner 微服务设计器 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/123729_b2fdfc35_4801041.png "API设计器中调试API.PNG")
2.  UniGenerator 微服务生成器 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/123753_34e21f02_4801041.png "genservice.png")
3.  UniStore 微服务库 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/123803_f86af795_4801041.png "servicestore.png")
4. UniAuth 统一认证平台
![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/163556_c9759463_4801041.png "uniauth.PNG")
5. UniGateway API网关
![输入图片说明](https://images.gitee.com/uploads/images/2020/0731/154824_93599fa9_4801041.png "UniGateway.PNG")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
